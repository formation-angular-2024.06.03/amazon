# Amazon

## Création des composants

Créer les composants suivants :

- `components/shared/navbar`
- `components/pages/home`
- `components/pages/productList`
- `components/pages/productDetails`

## Routage

- dans `app-routing.module.ts`, associer les pages à des chemins :
  - `/` => `HomeComponent`
  - `/products` => `ProductListComponent`
  - `/products/:id` => `ProductDetailsComponent`
- dans `app.component.html` :
  - afficher la navbar (ajouter `<app-navbar>...`)
  - afficher la page active (ajouter `<router-outlet>...`)
- dans `navbar.component.html`, ajouter des liens de navigation vers nos 2 pages (home et products)

## Page `productList`

- dans le `ts` :
  - créer un attribut avec la listes des produits
- dans le `html` :
  - afficher la liste des produits (avec un `*ngFor`)
  - pour chaque produit, ajouter un lien (ou un bouton) vers `/products/<id>`

## Page `productDetails`

- dans le `ts` :
  - injecter `ActivatedRoute`
  - récupérer l'id (avec snapshot ou observable)
- dans le `html` :
  - afficher l'id (et des données bidon si vous voulez)
  - ajouter un bouton pour revenir à la liste des produits

## Suppression des produits

- dans `services/product.service.ts` :
  - ajouter une méthode `deleteById(id: number): Observable<void>` qui envoie une requête DELETE sur l'url `products/<id>`
- dans `product-list.component.ts`, ajouter une méthode `onDelete(id: number)` qui appelle la méthode `deleteById` du service. Lorsque la suppression est effectuée (subscribe), il faudra supprimer le product de la liste.
- dans `product-list.component.html`, ajouter un bouton 'remove' pour chaque produit. Le clique sur le bouton devra appeler la méthode `onDelete` du ts.

## Amélioration de la page de détail

- dans `services/product.service.ts` :
  - ajouter une méthode `findById(id: number): Observable<Product>` qui envoie une requête GET sur l'url `products/<id>`
- dans `product-details.component.ts` :
  - ajouter un attribut `product`
  - dans le constructeur/ngOnInit, lorsqu'on récupère l'id du produit, appeler la méthode `findById` du service.
- dans `product-details.component.html`, afficher les détails du produit

## Amélioration la liste des produits

- créer un composant `ProductListItem` avec :
  - dans le `ts` :
    - un attribut `product` avec un `@Input`
    - un attribut `delete` avec `@Output` (l'initialiser avec un `new EventEmitter<?>()`)
    - ajouter une méthode `onDelete(id: number)` qui envoie l'event `delete` (`this.delete.emit(??)`)
  - dans le `html` :
    - copier/coller de `product-list.component.html`
- dans `product-list.component.html` :
  - remplacer l'affichage du produit par le composant `ProductListItem` en lui passant le product à afficher en input et en liant l'event `delete` à l'appel de la méthode `onDelete`

## Création de produits

- dans `services/product.service.ts` :
  - ajouter une méthode `save(product: Partial<Product>): Observable<Product>` qui envoie une requête POST sur l'url `products`
- créer un composant `ProductAdd` avec :
  - dans le `ts` :
    - créer un attribut `productForm` qui contiendra le `FormGroup` et les `FormControl` avec des contraintes de validation.
    - injecter le service `ProductService` dans le constructeur
    - ajouter une méthode `onSubmit` qui :
      - récupérer les données saisies par l'utilisateur (`productForm.value`)
      - appeler la méthode `save` du service
  - dans le `html` :
    - écrire le formulaire une balise `<form>` avec en attribut `[formGroup]="productForm"` et `(ngSubmit)="onSubmit()"`
    - contenant des `input` avec en attribut `formControlName="??"`
    - et un bouton "submit"
    - ajouter la validation (désactiver le bouton submit si le formulaire est invalide et afficher les erreurs de validation)
- dans `app-routing.module.ts`, associer la route `products/new` au composant `ProductAdd`
- dans `product-list.component.html` ajouter un lien de navigation
