import { Component } from '@angular/core';
import { AuthenticationService } from '../../../services/authentication.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrl: './login.component.css'
})
export class LoginComponent {

  email: string = '';
  password: string = '';

  constructor(private authenticationService: AuthenticationService) {}

  onSubmit() {
    this.authenticationService.login(this.email, this.password).subscribe();
  }

}
