import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ProductService } from '../../../services/product.service';

@Component({
  selector: 'app-product-add',
  templateUrl: './product-add.component.html',
  styleUrl: './product-add.component.css'
})
export class ProductAddComponent {

  productForm = new FormGroup({
    name: new FormControl('', {
      validators: [ Validators.required ],
      nonNullable: true
    }),
    price: new FormControl(0, {
      validators: [ Validators.min(0) ],
      nonNullable: true
    }),
    description: new FormControl('', {
      nonNullable: true
    })
  });

  constructor(private productService: ProductService) {}

  onSubmit() {
    if (this.productForm.valid)
      this.productService.save(this.productForm.value).subscribe();
    else
      this.productForm.markAllAsTouched();
  }

  get nameControl() {
    return this.productForm.get('name');
  }
}
