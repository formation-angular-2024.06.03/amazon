import { Component, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription, map, mergeMap } from 'rxjs';
import { ProductService } from '../../../services/product.service';
import { Product } from '../../../models/product.model';

@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.component.html',
  styleUrl: './product-details.component.css'
})
export class ProductDetailsComponent implements OnDestroy {

  product?: Product;

  private subscription: Subscription;

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private productService: ProductService
  ) {
    this.subscription = activatedRoute.params.pipe(
      map(params => Number(params['id'])),
      mergeMap(id => productService.findById(id))
    ).subscribe(p => this.product = p);
  }

  onBackClicked() {
    this.router.navigateByUrl('/products');
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
