import { Component, OnInit } from '@angular/core';
import { Product } from '../../../models/product.model';
import { Router } from '@angular/router';
import { ProductService } from '../../../services/product.service';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrl: './product-list.component.css'
})
export class ProductListComponent implements OnInit {

  products?: Product[];

  constructor(
    private router: Router,
    private productService: ProductService
  ) {}

  ngOnInit(): void {
    this.productService.findAll().subscribe(products => this.products = products);
  }

  onProductDetails(product: Product): void {
    this.router.navigateByUrl(`products/${product.id}`);
  }

  onDelete(id: number) {
    this.productService.deleteById(id).subscribe({
      next: () => this.products?.splice(this.products.findIndex(p => p.id === id), 1),
      error: err => console.log(err)
    }
      );
  }
}
