import { Component } from '@angular/core';
import { AuthenticationService, ConnectedUser } from '../../../services/authentication.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrl: './navbar.component.css'
})
export class NavbarComponent {

  user?: ConnectedUser;

  constructor(private authenticationService: AuthenticationService) {
    authenticationService.connectedUser.subscribe(u => this.user = u);
  }

  onLogoutClicked() {
    this.authenticationService.logout();
  }
}
