import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Product } from '../../../models/product.model';

@Component({
  selector: 'app-product-list-item',
  templateUrl: './product-list-item.component.html',
  styleUrl: './product-list-item.component.css'
})
export class ProductListItemComponent {

  @Input() product?: Product;
  @Output() delete: EventEmitter<number> = new EventEmitter<number>();

  onDeleteClicked(): void {
    this.delete.emit(this.product?.id);
  }
}
