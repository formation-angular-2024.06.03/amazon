import { APP_INITIALIZER, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './components/pages/home/home.component';
import { ProductListComponent } from './components/pages/product-list/product-list.component';
import { ProductDetailsComponent } from './components/pages/product-details/product-details.component';
import { NavbarComponent } from './components/shared/navbar/navbar.component';
import { HttpClientModule, provideHttpClient, withInterceptors } from '@angular/common/http';
import { ProductListItemComponent } from './components/shared/product-list-item/product-list-item.component';
import { ProductAddComponent } from './components/pages/product-add/product-add.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AuthenticationService } from './services/authentication.service';
import { LoginComponent } from './components/pages/login/login.component';
import { authenticationInterceptor } from './interceptors/authentication.interceptor';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    HomeComponent,
    ProductListComponent,
    ProductDetailsComponent,
    ProductListItemComponent,
    ProductAddComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule
  ],
  providers: [
    provideHttpClient(
      withInterceptors([authenticationInterceptor])
    ),
    {
      provide: APP_INITIALIZER,
      useFactory: (as: AuthenticationService) => () => as.init(),
      deps: [AuthenticationService],
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
