import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Product } from '../models/product.model';
import { Observable, map } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  readonly url = 'http://vps-3f8dfeab.vps.ovh.net:8080/products'

  constructor(
    private httpClient: HttpClient
  ) { }

  findAll(): Observable<Product[]> {
    return this.httpClient.get<Product[]>(this.url);
  }

  findById(id: number): Observable<Product> {
    return this.httpClient.get<Product>(this.url + '/' + id);
  }

  save(product: Partial<Product>): Observable<Product> {
    return this.httpClient.post<Product>(this.url, product);
  }

  deleteById(id: number): Observable<void> {
    return this.httpClient.delete<void>(this.url + '/' + id);
  }
}
