package fr.formation.amazon.controllers;

import java.util.Collection;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import fr.formation.amazon.exceptions.BadRequestException;
import fr.formation.amazon.exceptions.NotFoundException;
import fr.formation.amazon.models.Product;
import fr.formation.amazon.repositories.ProductRepository;
import jakarta.transaction.Transactional;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("products")
@RequiredArgsConstructor
public class ProductController {
    
    private final ProductRepository productRepository;

    @GetMapping
	public Collection<Product> findAll(@RequestParam(required = false) String q) {
		return q == null || q.isBlank()
			? productRepository.findAll()
			: productRepository.findByNameContaining(q);
	}

	@GetMapping("{id:\\d+}")
	public Product findById(@PathVariable long id) {
		return productRepository.findById(id)
            .orElseThrow(() -> new NotFoundException("no entity with id " + id + " exists"));
	}
	
	@PostMapping
	public Product save(@Valid @RequestBody Product m) {
		if (m.getId() != 0)
			throw new BadRequestException("a new entity cannot have a non-null id");
		return productRepository.save(m);
	}
	
	@PutMapping("{id:\\d+}")
	@Transactional
	public Product update(@PathVariable long id, @Valid @RequestBody Product product) {
		if (product.getId() != id)
			throw new BadRequestException("ids in url and body do no match");
		if (product.getId() == 0)
			throw new BadRequestException("id must not be zero");
        if (productRepository.findById(product.getId()).isEmpty())
			throw new NotFoundException("no entity with id " + product.getId() + " exists");
		return productRepository.save(product);
	}
	
	@DeleteMapping("{id:\\d+}")
	public void deleteById(@PathVariable long id) {
		productRepository.findById(id).ifPresentOrElse(
				productRepository::delete, 
				() -> { throw new NotFoundException("no entity with id " + id + " exists"); });
	}
}
