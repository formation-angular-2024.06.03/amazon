package fr.formation.amazon.repositories;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.formation.amazon.models.Product;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {

    Collection<Product> findByNameContaining(String q);

}
